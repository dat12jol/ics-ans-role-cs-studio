ics-ans-role-cs-studio
======================

Ansible role to install CS-Studio.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
csstudio_version: 4.5.0.0
csstudio_repository: production
csstudio_base_url: "https://artifactory.esss.lu.se/artifactory/CS-Studio"
csstudio_archive: "{{csstudio_base_url}}/{{csstudio_repository}}/{{csstudio_version}}/cs-studio-ess-{{csstudio_version}}-linux.gtk.x86_64.tar.gz"
csstudio_fonts: https://artifactory.esss.lu.se/artifactory/swi-pkg/fonts/cs-studio-fonts-20170412.tgz
csstudio_fonts_dir: /usr/share/fonts/ess
csstudio_xulrunner_version: 1.9.2.29pre
csstudio_xulrunner: https://artifactory.esss.lu.se/artifactory/swi-pkg/xulrunner/xulrunner-{{csstudio_xulrunner_version}}.en-US.linux-x86_64.tar.bz2
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-cs-studio
```

License
-------

BSD 2-clause
