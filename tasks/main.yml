---
- name: install required dependencies
  yum:
    name: "{{item}}"
    state: present
  with_items:
    - gnome-classic-session
    - bzip2
    # install fonts that have precedence on others to avoid kerning issues
    - urw-fonts

- name: check CS-Studio current version
  command: "cat {{csstudio_version_file}}"
  register: csstudio_current_version
  failed_when: False
  changed_when: False
  check_mode: no

- name: current CS-Studio version
  debug:
    msg: "CS-Studio current version: {{csstudio_current_version.stdout}}"

- name: remove previous backup if it exists
  file:
    path: "{{csstudio_directory}}-{{csstudio_current_version.stdout}}"
    state: absent
  when:
    - csstudio_current_version.stdout != ""
    - csstudio_current_version.stdout != csstudio_version

- name: backup current version
  command: "mv {{csstudio_directory}} {{csstudio_directory}}-{{csstudio_current_version.stdout}}"
  args:
    creates: "{{csstudio_directory}}-{{csstudio_current_version.stdout}}"
  when:
    - csstudio_current_version.stdout != ""
    - csstudio_current_version.stdout != csstudio_version

- name: install CS-Studio
  unarchive:
    src: "{{csstudio_archive}}"
    dest: /opt
    remote_src: yes
    owner: root
    group: root
    creates: "{{csstudio_directory}}"

- name: create the /usr/local/bin/css link
  file:
    src: "{{csstudio_directory}}/ESS CS-Studio"
    dest: /usr/local/bin/css
    state: link
    owner: root
    group: root

- name: create the /ess directory
  file:
    path: /ess
    state: directory
    owner: root
    group: root
    mode: 0755

- name: add CS-Studio to the desktop menu
  copy:
    src: "{{item.name}}"
    dest: "{{item.destination}}"
    owner: root
    group: root
    mode: 0644
  with_items:
    - name: ess-logo-square.png
      destination: /usr/local/share/icons
    - name: ESS.directory
      destination: /usr/share/desktop-directories
    - name: ESS-CS-Studio.desktop
      destination: /usr/share/applications
    - name: ESS.menu
      destination: /etc/xdg/menus/applications-merged

- name: install xulrunner
  unarchive:
    src: "{{ csstudio_xulrunner }}"
    dest: /opt
    remote_src: yes
    owner: root
    group: root

- name: create the /ess/xulrunner link for backward compatibility
  file:
    src: /opt/xulrunner
    dest: /ess/xulrunner
    state: link
    owner: root
    group: root

- name: create the CS-Studio fonts directory
  file:
    path: "{{ csstudio_fonts_dir }}"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: install CS-Studio fonts
  unarchive:
    src: "{{ csstudio_fonts }}"
    dest: "{{ csstudio_fonts_dir }}"
    remote_src: yes
    owner: root
    group: root
