import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_csstudio_version(File):
    version = File('/opt/cs-studio/ess-version.txt').content_string
    assert version.strip() == '4.5.0.0'


def test_csstudio_binary(File):
    binary = File('/usr/local/bin/css').linked_to
    assert binary == '/opt/cs-studio/ESS CS-Studio'
    assert File(binary).exists


def test_fonts(Command):
    cmd = Command('fc-match -s "monospace"')
    assert cmd.stdout.startswith('n022003l.pfb: "Nimbus Mono L" "Regular"')
    assert 'Open Sans' in cmd.stdout
    assert 'Titillium' in cmd.stdout


def test_xulrunner(Command):
    cmd = Command('/opt/xulrunner/xulrunner -v 2>&1')
    assert cmd.stdout.strip() == 'Mozilla XULRunner 1.9.2.29pre - 20120513033204'
